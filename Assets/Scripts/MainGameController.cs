﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameData;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class MainGameController : MonoBehaviour
{
  [SerializeField] private Text text_left_team_points  = null;
  [SerializeField] private Text text_right_team_points = null;

  public GameObject go_left_team  = null;
  public GameObject go_right_team = null;

  public GameObject lobby_view     = null;
  public GameObject go_battle_view = null;

  public GameObject prefab_battle = null;
  public GameObject go_battles_list = null;

  private UserOnClient user_on_client = null;


  // Use this for initialization
  private void Start()
  {
    MyDbg.writeLine = Debug.Log;
    user_on_client = new UserOnClient( (ulong)Random.Range( 1, 100 ) );

    initComponents();
  }

  private void initComponents()
  {
    user_on_client.onBattleChanged  += onBattleChanged;
    user_on_client.onAllBattles     += onAllBattles;
  }

  private void onAllBattles( List<BattleData> client_battles )
  {
    foreach ( Transform trans in go_battles_list.GetComponentsInChildren<Transform>().Where( t => t != go_battles_list.transform ) )
      Destroy( trans.gameObject );

    for ( int i = 0; i < client_battles.Count; ++i )
    {
      BattleData battle = client_battles[i];

      if ( battle.winner != BattleData.TeamIdx.NONE )
        return;

      GameObject go = Instantiate( prefab_battle, go_battles_list.transform );

      go.GetComponentInChildren<Text>().text = string.Format( "ID: {0}; left: {1}; right: {2}", battle.battle_id, battle.leftTeamScore, battle.rightTeamScore );
      go.transform.Find( "JoinLeft" ).GetComponent<Button>().onClick.AddListener( () => joinBattle( battle.battle_id, BattleData.TeamIdx.LEFT ) );
      go.transform.Find( "JoinRight" ).GetComponent<Button>().onClick.AddListener( () => joinBattle( battle.battle_id, BattleData.TeamIdx.RIGHT ) );

      go.transform.localPosition = Vector3.zero;
      go.transform.Translate( 0.0f, -15.0f * i, 0.0f );
    }
  }

  private void onBattleChanged()
  {
    ClientBattle battle = user_on_client.battle;
    if ( battle != null )
    {
      battle.onBattlePoints   += updateBattlePoints;
      battle.onPersonsChanged += updatePersons;

      updatePersons();
      updateBattlePoints();
    }

    go_battle_view.SetActive( battle != null );
    lobby_view    .SetActive( battle == null );
  }

  private void updatePersons()
  {
    foreach ( Transform trans in go_left_team.GetComponentsInChildren<Transform>().Where( t => t != go_left_team.transform ) )
      Destroy( trans.gameObject );
    foreach ( Transform trans in go_right_team.GetComponentsInChildren<Transform>().Where( t => t != go_right_team.transform ) )
      Destroy( trans.gameObject );

    ClientBattle battle = user_on_client.battle;

    if ( battle == null )
      return;

    int i = 1;
    foreach ( Person person in battle.leftTeam )
    {
      GameObject go = new GameObject( "person_view" );
      Text name = go.AddComponent<Text>();
      name.text = person.personName;
      name.font = Font.CreateDynamicFontFromOSFont( Font.GetOSInstalledFontNames()[0], 12 );

      go.transform.SetParent( go_left_team.transform, false );
      go.transform.Translate( 0.0f, -30.0f * i++, 0.0f );
    }

    i = 1;
    foreach ( Person person in battle.rightTeam )
    {
      GameObject go = new GameObject( "person_view" );
      Text name = go.AddComponent<Text>();
      name.text = person.personName;
      name.font = Font.CreateDynamicFontFromOSFont( Font.GetOSInstalledFontNames()[0], 12 );

      go.transform.SetParent( go_right_team.transform, false );
      go.transform.Translate( 0.0f, -30.0f * i++, 0.0f );
    }
  }

  private void updateBattlePoints()
  {
    ClientBattle battle = user_on_client.battle;

    if ( battle == null )
      return;

    text_left_team_points.text  = battle.leftTeamScore.ToString();
    text_right_team_points.text = battle.rightTeamScore.ToString();
  }

  private void Update()
  {
    CommandExecutor.execOneAction();
  }

  public void requestAllBattles()
  {
    user_on_client.sendRequsetAllBattles();
  }

  public void startNewBattle()
  {
    user_on_client.sendNewBattle();
  }

  private void joinBattle( int battle_id, BattleData.TeamIdx team_idx )
  {
    user_on_client.sendJoinBattle( battle_id, team_idx );
  }

  public void leaveBattle()
  {
    user_on_client.sendLeaveBattle();
  }

  public void clickInBattle()
  {
    user_on_client.sendClickInBattle();
  }
}
