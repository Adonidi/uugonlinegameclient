﻿using System;
using System.Collections.Generic;
using System.IO;
using GameData;


public partial class UserOnClient
{
  private void handleCommand( ushort command_num, Stream command_data )
  {
    CommandsServerToClient command = (CommandsServerToClient)command_num;
    BinaryReader br = new BinaryReader( command_data );

    MyDbg.writeLine( String.Format( "UserOnClient.handleCommand()   command_num {0}", Enum.GetName( typeof( CommandsServerToClient ), command ) ) );

    switch ( command )
    {
    case CommandsServerToClient.HANDSHAKED        : parseHandshaked( br );       break;
    case CommandsServerToClient.MESSAGE           : parseMessage( br );          break;
    case CommandsServerToClient.PERSON_ADDED      : parsePersonAdded( br );      break;
    case CommandsServerToClient.PERSON_REMOVED    : parsePersonRemoved( br );    break;
    case CommandsServerToClient.UPDATED_POINTS    : parseUpdatedPoints( br );    break;
    case CommandsServerToClient.BATTLE_FINISHED   : parseBattleFinished( br );   break;
    case CommandsServerToClient.BATTLE_JOINED     : parseBattleJoin( br );       break;
    case CommandsServerToClient.BATTLE_JOIN_FAILED: parseBattleJoinFailed( br ); break;
    case CommandsServerToClient.ALL_BATTLES       : parseAllBattles( br );       break;

    default:
      MyDbg.writeLine( "   Error: bad command!" );
      disconnect();
      break;
    }
  }

  private void parseAllBattles( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parseAllBattles()" );

    List<BattleData> battles = new List<BattleData>();
    int count = br.ReadInt32();
    for ( int i = 0; i < count; ++i )
    {
      BattleData b = new BattleData();
      b.fromBytesSimple( br );
      battles.Add( b );
    }

    CommandExecutor.addAction( () => onAllBattles( battles ) );
  }

  private void parseBattleJoinFailed( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parseBattleJoinFailed()" );
  }

  private void parseBattleFinished( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parseBattleFinished()" );

    if ( battle == null )
      return;

    BattleData.TeamIdx team_idx = (BattleData.TeamIdx)br.ReadByte();
    MyDbg.writeLine( string.Format( "   team_idx {0}", team_idx ) );

    battle = null;
    CommandExecutor.addAction( onBattleChanged );
  }

  private void parseUpdatedPoints( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parseUpdatedPoints()" );

    if ( battle == null )
      return;

    int left_team_score = br.ReadInt32();
    int right_team_score = br.ReadInt32();
    MyDbg.writeLine( string.Format("   left_team_score {0}  right_team_score {1}", left_team_score, right_team_score ) );

    battle.updatePoints( left_team_score, right_team_score );
  }

  private void parsePersonRemoved( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parsePersonRemoved()" );

    if ( battle == null )
      return;

    Person p = new ClientPerson( br );
    BattleData.TeamIdx team_idx = (BattleData.TeamIdx)br.ReadByte();

    battle.removePerson( p, team_idx );
  }

  private void parseBattleJoin( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parseBattleJoin()" );

    battle = new ClientBattle( br );
    team_idx = (BattleData.TeamIdx)br.ReadByte();

    CommandExecutor.addAction( onBattleChanged );
  }

  private void parsePersonAdded( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parsePersonAdded()" );

    Person p = new ClientPerson( br );
    BattleData.TeamIdx team_idx = (BattleData.TeamIdx)br.ReadByte();

    if ( battle == null )
      return;

    battle.addPerson( p, team_idx );
  }

  private void parseHandshaked( BinaryReader br )
  {
    MyDbg.writeLine( "UserOnClient.parseHandshaked()" );

    handshaked = true;

    person = new ClientPerson( br );

    CommandExecutor.addAction( onHandshaked );
  }

  private void parseMessage( BinaryReader command_data )
  {
    string msg = command_data.ReadString();

    MyDbg.writeLine( string.Format( "UserOnClient.parseMessage()   msg {0}", msg ) );
  }
}
