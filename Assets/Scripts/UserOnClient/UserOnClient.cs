﻿using System;
using System.Collections.Generic;
using System.Net;
using GameData;


public partial class UserOnClient
{
  private const int PORT = 11000;

  private readonly IOutConnector connector = null;

  private readonly ulong user_id = 0;
  private bool handshaked = false;

  public bool isConnected { get { return handshaked && !connector.isDead; } }

  public event Action onHandshaked                      = delegate { };
  public event Action onBattleChanged                   = delegate { };
  public event Action<List<BattleData>> onAllBattles  = delegate { };

  public ClientPerson person = null;
  public ClientBattle battle = null;
  public BattleData.TeamIdx team_idx = BattleData.TeamIdx.NONE;


  public UserOnClient( ulong user_id )
  {
    this.user_id = user_id;

    IPHostEntry ipHostInfo = Dns.GetHostEntry( Dns.GetHostName() );
    IPAddress ipAddress    = ipHostInfo.AddressList[0];
    connector              = ConnectionLib.ConnectorLibFactory.createOutConnection( ipAddress, PORT );

    connector.onConnected += onConnectorConnected;
    connector.onCommand   += handleCommand;

    connector.startWork();
  }

  public void disconnect()
  {
    handshaked = false;
    connector.close();
  }

  private void onConnectorConnected()
  {
    MyDbg.writeLine( "UserOnClient.onConnectorConnected()" );

    sendHandshake();
  }
}
