﻿using System;
using System.IO;
using GameData;


public partial class UserOnClient
{
  private void sendHandshake()
  {
    MyDbg.writeLine( "UserOnClient.sendHandshake()" );
    connector.send( (ushort)CommandsClientToServer.HANDSHAKE, stream =>
    {
      BinaryWriter bw = new BinaryWriter( stream );
      bw.Write( user_id );
    } );
  }

  public void sendMessage( string msg )
  {
    MyDbg.writeLine( String.Format( "UserOnClient.sendMessage()   msg {0}", msg ) );
    connector.send( (ushort)CommandsClientToServer.MESSAGE, stream =>
    {
      BinaryWriter bw = new BinaryWriter( stream );
      bw.Write( msg );
    } );
  }

  public void sendNewBattle()
  {
    MyDbg.writeLine( "UserOnClient.sendNewBattle()" );
    connector.send( (ushort)CommandsClientToServer.NEW_BATTLE, stream => { } );
  }

  public void sendJoinBattle( int battle_id, BattleData.TeamIdx team_idx )
  {
    MyDbg.writeLine( string.Format( "UserOnClient.sendJoinBattle()   battle_id {0}  team_idx {1}", battle_id, team_idx ) );
    connector.send( (ushort)CommandsClientToServer.JOIN_BATTLE, stream =>
    {
      BinaryWriter bw = new BinaryWriter( stream );
      bw.Write( battle_id );
      bw.Write( (byte)team_idx );
    } );
  }

  public void sendRequsetAllBattles()
  {
    MyDbg.writeLine( "UserOnClient.requsetAllBattles()" );
    connector.send( (ushort)CommandsClientToServer.REQUEST_ALL_BATTLES, stream => { } );
  }

  public void sendClickInBattle()
  {
    if ( battle == null || battle.winner != BattleData.TeamIdx.NONE )
      return;

    MyDbg.writeLine( "UserOnClient.clickInBattle()" );
    connector.send( (ushort)CommandsClientToServer.CLICK_IN_BATTLE, stream => { } );
  }

  public void sendLeaveBattle()
  {
    MyDbg.writeLine( "UserOnClient.sendLeaveBattle()" );

    battle = null;
    CommandExecutor.addAction( onBattleChanged );

    connector.send( (ushort)CommandsClientToServer.LEAVE_BATTLE, stream => { } );
  }
}
