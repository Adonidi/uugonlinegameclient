﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameData;


public class ClientBattle : BattleData
{
  public IEnumerable<Person> leftTeam  { get { return left_team; } }
  public IEnumerable<Person> rightTeam { get { return right_team; } }

  public event Action onBattlePoints   = delegate { };
  public event Action onPersonsChanged = delegate { };


  public ClientBattle()
    : base( 0 )
  {
  }

  public ClientBattle( int battle_id )
    : base( battle_id )
  {
  }

  public ClientBattle( BinaryReader br )
    : base( 0 )
  {
    fromBytes( br );
  }

  public new bool addPerson( Person person, TeamIdx team_idx )
  {
    if ( !base.addPerson( person, team_idx ) )
      return false;

    CommandExecutor.addAction( onPersonsChanged );
    return true;
  }

  public new void removePerson( Person person, TeamIdx team_idx )
  {
    base.removePerson( person, team_idx );
    CommandExecutor.addAction( onPersonsChanged );
  }

  public new void updatePoints( int left_team_score, int right_team_score )
  {
    base.updatePoints( left_team_score, right_team_score );
    CommandExecutor.addAction( onBattlePoints );
  }
}
