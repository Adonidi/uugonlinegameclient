﻿using System;
using System.Collections.Generic;


public static class CommandExecutor
{
  private static readonly Queue<Action> actions = new Queue<Action>();
  private static readonly object lock_obj = new object();


  public static void addAction( Action act )
  {
    lock ( lock_obj )
    {
      actions.Enqueue( act );
    }
  }

  public static bool execOneAction()
  {
    Action act = null;
    lock ( lock_obj )
    {
      if ( actions.Count > 0 )
        act = actions.Dequeue();
    }
    if ( act == null )
      return false;

    act();
    return true;
  }
}
